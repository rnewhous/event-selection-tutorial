# Specify the image and release tag from which we're working
FROM atlas/analysisbase:21.2.85-centos7

# Put the current repo (the one in which this Dockerfile resides) in the /Tutorial directory
# Note that this directory is created on the fly and does not need to reside in the repo already
ADD . /Tutorial

# Go into the /Tutorial/build directory and make /Tutorial/build the default working directory (again, it will create the directory if it doesn't already exist)
WORKDIR /Tutorial/build

# Create a run directory (note: atlas user needs sudo privileges to make directories)
RUN sudo mkdir -p /Tutorial/run

# Source the ATLAS analysis environment
# Make sure the directory containing your analysis code (and the code inside it) is owned by atlas user
# Build your source code using cmake
RUN source ~/release_setup.sh &&  \
    sudo chown -R atlas /Tutorial && \
    cmake ../source && \
    make

# Add atlas user to root group (needed to avoid permissions issues when writing files on the local machine)
RUN source ~/release_setup.sh && sudo usermod -aG root atlas
